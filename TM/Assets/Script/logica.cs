﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;

}




public class ClassResposta : Quiz
{// classe resposta
    public bool ecorreta;
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;


    }

}// fim classe resposta


public class ClassPergunta : Quiz
{// classe perguntas
    
    public int pontos;
    public ClassResposta[] respostas;


// METODO É PARA CRIAR AS PERGUNTAS

    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;



     // METODO PARA ADD RESPOSTA
    }
    public void AddReposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }
        for(int i = 0; i < respostas.Length; i++)
        {
            if(respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }

        }

    } //fim METODO PARA ADD RESPOSTA




    //LISTA OU EXIBIR AS RESPOSTAS
    public void ListarRespostas()
    {
        if(respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);

            }
        }

    }
    //fim LISTA OU EXIBIR AS RESPOSTAS


    //CHECAR RESPOSTA CORRETA
    public bool ChecarRespostaCorreta (int pos)
    {
        if( respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }
    //FIM RESPOSTA CORRETA






}// fim classe perguntas






// CLASS LÓGICA - PRINCIPAL
public class logica : MonoBehaviour

{ //INICIO CLASS LOGICA


    public Text tituloPergunta;
    public Button[] respostasBtn = new Button[4];
    public List<ClassPergunta> PerguntaLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;
    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;

    void Start()
    { // METODO START
      //string texto = pergunta.text;
      // Debug.Log(texto);


        ClassPergunta p1 = new ClassPergunta("1. Quem criou a lógica?", 5);
        p1.AddReposta("Platão",false);
        p1.AddReposta("Aristóteles", true);
        p1.AddReposta("Sócrates", false);
        p1.AddReposta("Tales de Mileto", false);

        ClassPergunta p2 = new ClassPergunta("2. O que é lógica?", 3);
        p2.AddReposta("É o estudo dos conhecimentos matemáticos complexos.", false);
        p2.AddReposta("Conhecimento inato do ser humano.", false);
        p2.AddReposta("Parte da filosofia que estuda os mitos.", true);
        p2.AddReposta("É uma parte da filosofia que estuda o fundamento, a estrutura e as expressões humanas do conhecimento.", false);


        ClassPergunta p3 = new ClassPergunta("3. O que é um algoritmo?", 5);
        p3.AddReposta("É uma receita que mostra passo a passo os procedimentos necessários para a resolução de uma tarefa", true);
        p3.AddReposta("São números matemáticos", false);
        p3.AddReposta("São expressões matemáticas complexas", false);
        p3.AddReposta("É uma receita que mostra o que fazer em um problema.", false);

        ClassPergunta p4 = new ClassPergunta("4. Qual dessas palavras não é um tipo de algoritmo?", 4);
        p4.AddReposta("Pseudocódigo", false);
        p4.AddReposta("Fluxograma", false);
        p4.AddReposta("Descrição intuitiva", true);
        p4.AddReposta("Diagrama de Chapin", false);

        ClassPergunta p5 = new ClassPergunta("5. O que é uma variável", 5);
        p5.AddReposta("É um local na memória destinado a guardar um cálculo informado pelo usuário.", false);
        p5.AddReposta("É um objeto (uma posição, frequentemente localizada na memória) capaz de reter e representar um valor ou expressão.", true);
        p5.AddReposta("São onde os cálculos realizados pelos processadores são armazenados", false);
        p5.AddReposta("É uma equação matemática que armazena os números irracionais.", false);

        ClassPergunta p6 = new ClassPergunta("6. Dentre as alternativas abaixo assinale a que não é considerada um dos tipos básicos de dados que a linguagem de programação irá manipular.", 5);
        p6.AddReposta("Dados alfanumérico", false);
        p6.AddReposta("Dados Lógicos", false);
        p6.AddReposta("Dados numéricos", false);
        p6.AddReposta("Dados vitais", true);

        ClassPergunta p7 = new ClassPergunta("7. Dentre as palavras abaixo qual não é considerada um tipo de operador em programação?", 4);
        p7.AddReposta("Lógicos", false);
        p7.AddReposta("Aritméticos", false);
        p7.AddReposta("Descrição", true);
        p7.AddReposta("Relacionais", false);

        ClassPergunta p8 = new ClassPergunta("8. Os dados lógicos podem assumirem que tipo de valores?", 5);
        p8.AddReposta("3,15,103", false);
        p8.AddReposta("'A, B, C, D'", false);
        p8.AddReposta("Verdadeiro e falso", true);
        p8.AddReposta("2,14 3,14 -58", false);

        ClassPergunta p9 = new ClassPergunta("9. Qual é o símbolo usado no visualG para atribuição?", 3);
        p9.AddReposta("<", false);
        p9.AddReposta("=", false);
        p9.AddReposta("<=", false);
        p9.AddReposta("<-", true);

        ClassPergunta p10 = new ClassPergunta("10.	Qual é o símbolo do operador relacional no visualG para - diferente?", 3);
        p10.AddReposta("<>", true);
        p10.AddReposta("=/-", false);
        p10.AddReposta("<=", false);
        p10.AddReposta(">=", false);


        PerguntaLista.Add(p1);
        PerguntaLista.Add(p2);
        PerguntaLista.Add(p3);
        PerguntaLista.Add(p4);
        PerguntaLista.Add(p5);
        PerguntaLista.Add(p6);
        PerguntaLista.Add(p7);
        PerguntaLista.Add(p8);
        PerguntaLista.Add(p9);
        PerguntaLista.Add(p10);
        

        perguntaAtual = p1;

        exibirPerguntasNoQuiz();


    }// FIM METODO START 

    void Update()
    {
       if(carregarProximaPergunta == true)
       {
            tempo += Time.deltaTime;
            if(tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
                    


            }
       }
    }





    private void exibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostasBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostasBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostasBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostasBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;

    }

    public void ProximaPergunta()
    {

        contagemPerguntas++;
        if (contagemPerguntas < PerguntaLista.Count)
        {
            perguntaAtual = PerguntaLista[contagemPerguntas];
            exibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }

    }
    
    public void ClickResposta(int pos)
    {
        if(perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta Certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
        }
        else
        {
            //Debug.Log("Resposta Errada!!!");
            PainelErrado.SetActive(true);
            
        }
        
        
    }
    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }
    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");      
    }
    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }
    public void ComoJogar()
    {
        SceneManager.LoadScene("ComoJogar");
    }
    public void SairJogo()
    {
        Application.Quit();
    }






}
//FIM CLASS LOGICA