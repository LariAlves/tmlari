﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinalAcerto;
    public GameObject PainelFinalErrou;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1) " + "Quem sabe o príncipe virou um... - Malandragem - Cássia Eller", 5 );
        p1.AddResposta("CHATO", true);
        p1.AddResposta("RATO", false);
        p1.AddResposta("SAPO", false);
        p1.AddResposta("CARRAPATO", false);

        ClassPergunta p2 = new ClassPergunta("2) " + "A minha alma tá armada e apontada para a cara do ... - Minha Alma - O ", 5);
        p2.AddResposta("SUJEITO", false);
        p2.AddResposta("SUSPEITO", false);
        p2.AddResposta("SOSSEGO", true);
        p2.AddResposta("JOÃO PEDRO", false);

        ClassPergunta p3 = new ClassPergunta("3) " + "Um dia eu vou estar ... - Você vai estar na minha - Negra Li", 6);
        p3.AddResposta("na rua e você vai estar na esquina", false);
        p3.AddResposta("na tua e você vai estar na minha ", false);
        p3.AddResposta("na sua e você vai estar na minha", false);
        p3.AddResposta("na tua e você vai estar na mira", true);

        ClassPergunta p4 = new ClassPergunta("4) " + "Meu namoro é na ... - Mulher de Fases -Raimundos", 2);
        p4.AddResposta("Folhinha", true);
        p4.AddResposta("Folia", false);
        p4.AddResposta("Avenida", false);
        p4.AddResposta("Colina", false);

        ClassPergunta p5 = new ClassPergunta("5) " + "Eu perguntava ... - Whisky a Go-Go - Roupa Nova ", 5);
        p5.AddResposta("Tudo em holandês", false);
        p5.AddResposta("Tudo em português", false);
        p5.AddResposta("Do you wanna dance?", true);
        p5.AddResposta("Do you holandês", false);

        ClassPergunta p6 = new ClassPergunta("6) " + "Hoje é domingo,... - Hoje é Domingo", 5);
        p6.AddResposta("Pé de cachimbo", false);
        p6.AddResposta("Pé e cachimbo", false);
        p6.AddResposta("Pede cachimbo", true);
        p6.AddResposta("Pede carimbo", false);

        ClassPergunta p7 = new ClassPergunta("7) " + "Se essa rua, se essa rua fosse minha, eu mandava, eu mandava ela ... - Se Essa Rua Fosse Minha", 5);
        p7.AddResposta("Ladrilhar", true);
        p7.AddResposta("Ela brilhar", false);
        p7.AddResposta("Ela piscas", false);
        p7.AddResposta("Ela dançar", false);

        ClassPergunta p8 = new ClassPergunta("8) " + "Água mineral, ... Você vai ficar legal - Água Mineral - Carlinhos Brown", 5);
        p8.AddResposta("no carnaval", false);
        p8.AddResposta("do Candeal", true);
        p8.AddResposta("o que é que há? ", false);
        p8.AddResposta("que animal", false);

        ClassPergunta p9 = new ClassPergunta("9) " + " Arerê ...  - Arerê Ivete Sangalo", 5);
        p9.AddResposta("Um hobby, um lobby, um love com você ", false);
        p9.AddResposta("Um love, um hobby, um lobby com você ", false);
        p9.AddResposta("Um love, um love, um love com você", false);
        p9.AddResposta("Um lobby, um hobby, um love com você", true);

        ClassPergunta p10 = new ClassPergunta("10) " + " Arerê ...  - Arerê Ivete Sangalo", 5);
        p10.AddResposta("ousadia ", false);
        p10.AddResposta("ouça, diga ", true);
        p10.AddResposta("outro dia", false);
        p10.AddResposta("outra vida ", false);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            
            if (pontos >= 10)
            {
                PainelFinalAcerto.SetActive(true);
                PainelFinalAcerto.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
            }
            else
            {
                PainelFinalErrou.SetActive(true);
                PainelFinalErrou.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
            }

            
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
